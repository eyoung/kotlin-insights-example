package com.madsciencesoftware.kotlinplayground

/**
 * Created by eyoung on 3/24/16.
 */
data class FancyData(val mText: String, val mColor: Int)