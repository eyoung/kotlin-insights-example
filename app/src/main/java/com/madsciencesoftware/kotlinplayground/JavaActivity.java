package com.madsciencesoftware.kotlinplayground;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by eyoung on 4/5/16.
 */
public class JavaActivity extends AppCompatActivity {
    private LinearLayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView helloWorldText = (TextView) findViewById(R.id.helloWorldText);
        helloWorldText.setText("こんにちは、世界");

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        KotlinAdapter adapter = new KotlinAdapter();
        recyclerView.setAdapter(adapter);

        ArrayList<FancyData> list = new ArrayList<>();
        list.add(new FancyData("Blue", Color.BLUE));
        list.add(new FancyData("Green", Color.GREEN));
        list.add(new FancyData("Yellow", Color.YELLOW));
        adapter.setData(list);
    }
}
