package com.madsciencesoftware.kotlinplayground

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var mLayoutManager: LinearLayoutManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        helloWorldText.text = "こんにちは、世界"
        recyclerView.setHasFixedSize(true)
        mLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = mLayoutManager
        val adapter = KotlinAdapter()
        recyclerView.adapter = adapter
        val fancyData1 = FancyData("Blue", Color.BLUE)
        val fancyData2 = FancyData("Green", Color.GREEN)
        val fancyData3 = FancyData("Sky", Color.BLUE)
        val fancyData4 = FancyData("Apple", Color.RED)
        val list = listOf(fancyData1, fancyData2, fancyData3, fancyData4)
//        adapter.data = list.filter { item -> item.mColor == Color.BLUE }.sortedBy { item -> item.mText }
        adapter.data = list

        button.setOnClickListener {
            val intent = Intent(this, JavaActivity::class.java)
            startActivity(intent)
        }
    }
}
