package com.madsciencesoftware.kotlinplayground

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.data_view.view.*

/**
 * Created by eyoung on 3/24/16.
 */
class KotlinAdapter : RecyclerView.Adapter<KotlinAdapter.KotlinViewHolder>() {

    var data: List<FancyData>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onBindViewHolder(holder: KotlinViewHolder?, position: Int) {
        val fancyData = data?.getOrNull(position)?: FancyData("Out of bounds data $position", Color.RED)
        holder?.mTextView?.text = fancyData.mText
        holder?.mTextView?.setTextColor(fancyData.mColor)
    }

    override fun getItemCount(): Int {
        return data?.size?: 0
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): KotlinViewHolder? {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.data_view, parent, false)
        return KotlinViewHolder(view)
    }

    class KotlinViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val mTextView = itemView?.dataText
    }
}